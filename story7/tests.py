from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from .views import index

class UnitTestStory7(TestCase):
    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_landing_page_is_written(self):
        self.assertIsNotNone(index)
    def test_function_story7(self):
        found = resolve('/')
        self.assertEqual(found.func, index)